import React, { Component } from "react";
import FormSearch from "./Components/FormSearch";
import Header from "./Components/Header";
import TableData from "./Components/TableData";
import AddUser from "./Components/AddUser";
import Data from "./Data.json";
import { v1 as uuidv1, validate } from 'uuid'
class AppNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trangthai: false,
      data: [],
      searchText: "",
      editUser:false,
      editUserData:{},
    };
  }
  
  componentWillMount() {
    if(localStorage.getItem("UserData")=== null){
      localStorage.setItem("UserData", JSON.stringify(Data))
    }
    else{
      var temp = JSON.parse(localStorage.getItem('UserData'))
      this.setState({
        data: temp
      });
    }
  }
  
  setTrangthai = () => {
    this.setState({
      trangthai: !this.state.trangthai,
    });
  };
  connectData = (data) => {
    this.setState({
      searchText: data,
    });
    console.log(data);
  };
  addDataUser =(name,tel,permitsion) =>{
    var item = {}
    item.id = uuidv1()
    item.name = name
    item.tel = tel
    item.permitsion = permitsion
    var newData =this.state.data
      newData.push(item)
      this.setState({
        data:newData
      });
      localStorage.setItem('UserData',JSON.stringify(newData))
  }
  editUser = (user) =>{
    this.setState({
      editUserData: user
    });
  }
  setEditUser = () =>{
    this.setState({
      editUser:!this.state.editUser
    });
  }
  getUserObj =(info)=>{
   this.state.data.map((value,key)=>{
     if(value.id === info.id) {
       value.name = info.name
       value.tel = info.tel
       value.permitsion = info.permitsion
     }
   })
   localStorage.setItem('UserData',JSON.stringify(this.state.data))
  }
  deleteFunc = (id) =>{
   var temData = this.state.data
  temData= temData.filter(item => item.id !== id)
   this.setState({
     data:temData
   });
   localStorage.setItem('UserData',JSON.stringify(temData))
  }
  render() {
    var searchResult = [];
    this.state.data.forEach((item) => {
      if (item.name.indexOf(this.state.searchText) !== -1) {
        searchResult.push(item);
      }
    });
    return (
      <div>
        <Header />
        <div className="searchForm">
          <div className="container">
            <div className="row">
              <FormSearch ConnectData={(data) => this.connectData(data)} 
              editUserStatus={this.state.editUser} 
              SetEditUser={()=>this.setEditUser()}
              editUserDatas ={this.state.editUserData}
              getUserObj = {(info)=>this.getUserObj(info)} />
         
              <TableData Data={searchResult} 
              editfunc={(user)=>this.editUser(user)}
              SetEditUser={()=>this.setEditUser()}
              deleteFunc ={(id)=>this.deleteFunc(id)}/>
              <AddUser
                trangthai={this.state.trangthai}
                setTrangthai={() => this.setTrangthai()}
                addUser = {(name,tel,permitsion)=>this.addDataUser(name,tel,permitsion)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AppNew;
