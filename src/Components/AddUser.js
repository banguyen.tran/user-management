import React, { Component } from 'react';

class AddUser extends Component {
    constructor(props) {
        super(props);
        this.state ={
          id:'',
          name:'',
          tel:'',
          permitsion:''
        }
      }
    isChange =(event)=>{
      const name = event.target.name
      const value = event.target.value
      this.setState({
       [name]:value
    })
    // var item = {}
    // item.id = this.state.id
    // item.name = this.state.name
    // item.tel = this.state.tel
    // item.permitsion = this.state.permitsion
    // console.log(item)
  }
        hienThiNut =() =>{
            if (this.props.trangthai) {
                return (
                    <div className="btn btn-block btn-outline-secondary" onClick={()=>this.props.setTrangthai()}>Dong lai</div>
                )
            }
            else {
                return(
                    <div className="btn btn-block btn-outline-info" onClick={()=>this.props.setTrangthai()}>Them moi</div>
                )}
           }
        
       hienthiForm = ()=>{
           if(this.props.trangthai) {
               return(
            <div className="col" className="card border-primary mb-3 mt-2">
              <form>
              <div className="card-header">Them moi</div>
              <div className="card-body text-primary">
                <div className="form-group">
                  <input type="text" className="form-control" name="name" onChange={(event)=>this.isChange(event)} aria-describedby="helpId" placeholder="Ten User" />
                </div>
                <div className="form-group">
                  <input type="text" className="form-control" name="tel" onChange={(event)=>this.isChange(event)} aria-describedby="helpId" placeholder="Dien thoai" />
                </div>
                <div className="form-group">
                  <select className="custom-select" name="permitsion" onChange={(event)=>this.isChange(event)} >
                    <option >Chon quyen</option>
                    <option value={1}>Admin</option>
                    <option value={2}>Moderator</option>
                    <option value={3}>Member</option>
                  </select>
                </div>
                <div className="form-group">
                   <input type="reset" value="Them moi" className="btn btn-block btn-primary" onClick={(name,tel,permitsion)=>this.props.addUser(this.state.name,this.state.tel,this.state.permitsion)}/>
                </div>
              </div>
              </form>
            </div>
               )
           }
       }
    render() {
      
        return (
            <div >
                 {this.hienThiNut()}
                 {this.hienthiForm()}
          </div>
        );
    }
}

export default AddUser;