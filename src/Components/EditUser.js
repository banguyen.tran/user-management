import React, { Component } from 'react';

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state ={
      id: this.props.EditUserDatas.id,
      name: this.props.EditUserDatas.name,
      tel: this.props.EditUserDatas.tel,
      permitsion: this.props.EditUserDatas.permitsion
    }
    //getUserInfo
  }
  isChang =(event) =>{
    const name = event.target.name
    const value = event.target.value
    this.setState({
      [name]: value
    });
  }
  saveButton = ()=>{
    var info = {}
    info.id = this.state.id
     info.name = this.state.name
     info.tel = this.state.tel
     info.permitsion = this.state.permitsion
    this.props.getUserInfo(info)
    this.props.setEditUserFunc()
  }
    render() {
        return (
            <div className="col" >
            <form method="post" >
                <div className="card-text text-white bg-warning mb-3 mt-2">
            <div className="card-header text-center">Chinh sua User</div>
            <div className="card-body text-primary">
              <div className="form-group">
                <input type="text" onChange={(event)=>this.isChang(event)} defaultValue={this.props.EditUserDatas.name} className="form-control" name="name"  aria-describedby="helpId" placeholder="Ten User" />
              </div>
              <div className="form-group">
                <input onChange={(event)=>this.isChang(event)} defaultValue={this.props.EditUserDatas.tel} type="text" className="form-control" name="tel"  aria-describedby="helpId" placeholder="Dien thoai" />
              </div>
              <div className="form-group">
                <select onChange={(event)=>this.isChang(event)} className="custom-select" defaultValue={this.props.EditUserDatas.permitsion} name="permitsion"  >
                  <option >Quyen</option>
                  <option value={1}>Admin</option>
                  <option value={2}>Moderator</option>
                  <option value={3}>Member</option>
                </select>
              </div>
              <div className="form-group">
                 <input type="button" value="Luu" className="btn btn-block btn-primary bg-danger" onClick={()=>this.saveButton()}/>
              </div>
            </div>
            </div>
            </form>
          </div>
        );
    }
}

export default EditUser;