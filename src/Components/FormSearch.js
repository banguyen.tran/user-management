import React, { Component } from 'react';
import EditUser from "./EditUser"
class FormSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textValue : "",
      editUserObj:{}
    }
    
  }
  getUserInfo = (info)=>{
    this.setState({
      editUserObj: info
    });
    this.props.getUserObj(info)
  }
  isChange = (event) =>{
    console.log(event.target.value)
    this.setState({
      textValue: event.target.value
    });
    this.props.ConnectData(this.state.textValue)
  }
  checkEditUserForm = ()=>{
    if(this.props.editUserStatus) {
      return(<EditUser setEditUserFunc={()=>this.props.SetEditUser()}
       EditUserDatas ={this.props.editUserDatas} 
       getUserInfo={(info)=>this.getUserInfo(info)}/>)
    }
  }
    render() {
        return (
            <div className="col-12">
              <div className="row">
              <div className="form-group">
               {this.checkEditUserForm()}
          <div className="btn-group" style={{width: '500px',textAlign:""}}>
            <input type="text" className="form-control" placeholder="Nhap ten can tim" onChange={(event)=>this.isChange(event)} />
            <div className="btn btn-info" onClick= {(data)=> this.props.ConnectData(this.state.textValue)}>Tìm</div>
          </div>
        </div>
        <hr className="my-2"/>
              </div>
      </div>
        );
    }
}

export default FormSearch;
