import React, { Component } from 'react';
import TableDataRow from './TableDataRow';
class TableData extends Component {
  constructor(props) {
    super(props);
    
  }
  deleteButtonEdit = (id) => {
    this.props.deleteFunc(id)
  }
    render() {
        return (
            <div className="col">
        <table className="table table-striped table-hover table-{1|striped|sm|bordered|hover|inverse}">
          <thead className="thead-inverse|thead-default">
            <tr>
              <th>STT</th>
              <th>Tên</th>
              <th>Điện thoại</th>
              <th>Quyền</th>
              <th>Thao tác</th>
            </tr>
          </thead> 
          <tbody>
          {this.props.Data.map((value,key) =>{
           return(
           <TableDataRow key ={key} Stt={key}
           Names={value.name} 
           Permisions={value.permitsion} 
           Phone ={value.tel} 
           id = {value.id}
           editFunction={(user)=>this.props.editfunc(value)} 
           setEditUserfun ={()=>this.props.SetEditUser()}
           deleteButtonEdit={(id)=>this.deleteButtonEdit(id)}/>) 
             })
          }
          </tbody>  
        </table>
      </div>
        );
    }
}

export default TableData;