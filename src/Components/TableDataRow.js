import React, { Component } from 'react';

class TableDataRow extends Component {
    constructor(props) {
        super(props);
        
    }
    permision = () =>{
        if(this.props.Permision === 1) {
            return "Admin"
        }
        else if(this.props.Permision === 2) {
            return "Moderator"
        }
        else {
            return "Member"
        }
      }
      editClick = () => {
        this.props.editFunction()
        this.props.setEditUserfun()
      }
      deleteButton = (id) =>{
        this.props.deleteButtonEdit(id)
      }
    render() {
        return (
            <tr>
            <td >{this.props.Stt + 1}</td>
            <td> {this.props.Names} </td>
            <td>{this.props.Phone}</td>
            <td>{this.permision()}</td>
            <td>
              <div className="btn btn-group">
                <div className="btn btn-warning sua"><i className="fa fa-edit" onClick={()=>{this.editClick()}} >Sửa</i> </div>
                <div className="btn btn-danger xoa"><i className="far fa-trash-alt" onClick={(id)=>this.deleteButton(this.props.id)}> Xóa</i></div>
              </div>
            </td>
          </tr>
            
        );
    }
}

export default TableDataRow;